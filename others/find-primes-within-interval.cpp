#include <iostream>
#include <vector>

using namespace std;

typedef long long ll;
ll N = 1e7+10;

void sieveOfEratosthenes(ll n, std::vector<bool>& isPrime) {
    isPrime[0] = isPrime[1] = false;

    for (ll i = 2; i * i <= n; ++i) {
        if (isPrime[i]) {
            for (ll j = i * i; j <= n; j += i) {
                isPrime[j] = false;
            }
        }
    }
}

void findPrimesInRange(ll m, ll n, const std::vector<bool>& isPrime) {
    for (ll i = m; i <= n; ++i) {
        if (isPrime[i]) {
            std::cout << i << " ";
        }
    }
    std::cout << std::endl;
}

int main()
{
    std::vector<bool> isPrime(N, true);
	sieveOfEratosthenes(N, isPrime);
	
	ll t;
	cin >> t;
	while (t--) {
		ll m, n;
		cin >> m >> n;
		findPrimesInRange(m, n, isPrime);
	}

	return 0;
}
