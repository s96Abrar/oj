#include <iostream>

template<typename T>
class Heap {
private:
	int capacity;
	int size;
	T *items;
	bool (*compare)(T num1, T num2);

	void deallocateMemory() {
		delete[] items;
	}

	inline int getParentIndex(int childIndex) {
		return (childIndex - 1) / 2;
	}

	inline int getLeftChildIndex(int parentIndex) {
		return (2 * parentIndex) + 1;
	}

	inline int getRightChildIndex(int parentIndex) {
		return (2 * parentIndex) + 2;
	}

	inline bool hasParent(int index) {
		return getParentIndex(index) >= 0;
	}

	inline bool hasLeftChild(int index) {
		return getLeftChildIndex(index) < size;
	}

	inline bool hasRightChild(int index) {
		return getRightChildIndex(index) < size;
	}

	inline T parent(int index) {
		return items[getParentIndex(index)];
	}

	inline T leftChild(int index) {
		return items[getLeftChildIndex(index)];
	}

	inline T rightChild(int index) {
		return items[getRightChildIndex(index)];
	}

	void swap(int index1, int index2) {
		std::swap(items[index1], items[index2]);
	}

	void ensureCapacity() {
		if (size == capacity) {
			capacity *= 2;
			T *newItems = new T[capacity];
			
			for (int i = 0; i < size; ++i) {
				newItems[i] = items[i];
			}
			
			deallocateMemory();

			items = newItems;
		}
	}

	void heapifyUp(int index) {
		while (hasParent(index) && compare(items[index], parent(index))) {
			swap(getParentIndex(index), index);
			index = getParentIndex(index);
		}
	}

	void heapifyDown(int index) {
		while (hasLeftChild(index)) {
			int smallerIndex = getLeftChildIndex(index);
			if (hasRightChild(index) 
				&& compare(rightChild(index), leftChild(index))) {
				smallerIndex = getRightChildIndex(index);
			}

			if (compare(items[index], items[smallerIndex])) break;

			swap(index, smallerIndex);
			index = smallerIndex;
		}
	}

public:
	explicit Heap(int capacity, bool (*compare)(T num1, T num2))
		: capacity(capacity), size(0), compare(compare)
	{
		items = new T[capacity];
	}

	~Heap() {
		deallocateMemory();
	}

	bool isEmpty() {
		return size == 0;
	}

	void add(T data) {
		ensureCapacity();

		items[size++] = data;
		
		heapifyUp(size - 1);
	}

	T top() {
		if (isEmpty()) {
			throw std::runtime_error("Heap is empty");
		}
		return items[0];
	}

	T pop() {
		if (isEmpty()) {
			throw std::runtime_error("Heap is empty");
		}

		T item = items[0];
		items[0] = items[--size];

		heapifyDown(0);
		
		return item;
	}
};

bool cmp1(int num1, int num2) {
	return num1 < num2;
}

bool cmp2(int num1, int num2) {
	return num1 > num2;
}

int main()
{
	Heap<int> h(10, &cmp2);

	h.add(10);
	h.add(9);
	h.add(8);
	h.add(11);
	h.add(1);

	while (!h.isEmpty()) {
		std::cout << h.pop() << " ";
	}
	std::cout << "\n";

	return 0;
}
