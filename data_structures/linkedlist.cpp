#include <iostream>

template<typename T>
class LinkedList {

	class Node {
	public:
		T data;
		Node* next;
		Node* prev;

		Node(T data) : data(data), next(nullptr), prev(nullptr) {}
	};

private:
	Node* head;
	Node* tail;
	int size = 0;

public:
	LinkedList() : head(nullptr), tail(nullptr) {}
	~LinkedList() {
		Node* curr = head;
		Node* temp;
		while (curr) {
			temp = curr;
			curr = curr->next;
			delete temp;
		}
	}

	void insertAtFirst(T data) {
		Node *node = new Node(data);
		if (isEmpty()) {
			head = node;
			tail = node;
		} else {
			head->prev = node;
			node->next = head;
			head = node;
		}
		++size;
	}

	void insertAtLast(T data) {
		Node *node = new Node(data);
		if (isEmpty()) {
			head = node;
			tail = node;
		} else {
			tail->next = node;
			node->prev = tail;
			tail = node;
		}
		++size;
	}

	bool isEmpty() {
		return head == nullptr;
	}

	void print() {
		Node *curr = head;
		while (curr) {
			std::cout << curr->data << " ";
			curr = curr->next;
		}
		std::cout << "\n";
	}
};

int main()
{
	LinkedList<int> l;
	l.insertAtLast(3);
	l.insertAtFirst(2);
	l.insertAtFirst(1);

	l.print();

	return 0;
}
