#include <iostream>
#include <string>

using namespace std;

class TrieNode {
private:
	TrieNode* children[26] = {nullptr}; // number of alphabets
	int size = 0;

	void add(const string& str, int index) {
		++size;
		
		if (index == (int) str.size()) return;

		int i = str[index] - 'a';

		TrieNode* child = children[i];
		if (!child) {
			child = new TrieNode;
			children[i] = child;
		}

		child->add(str, index + 1);
	}

	int findCount(const string& str, int index) {
		if (index == (int) str.size()) return size;

		int i = str[index] - 'a';
		TrieNode* child = children[i];
		if (!child) return 0;

		return child->findCount(str, index + 1);
	}

	void deallocate(TrieNode* node) {
		for (auto child : children) {
			delete child;
		}
	}

public:
	TrieNode() {
		for (int i = 0; i < 26; ++i) {
			children[i] = nullptr;
		}
	}

	~TrieNode() {
		deallocate(this);
	}

	void add(const string& str) {
		add(str, 0);
	}

	int find(const string& str) {
		return findCount(str, 0);
	}
};