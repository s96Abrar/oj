#include <iostream>
#include <stack>

using namespace std;

class Queue {
private: 
    stack<int> s1, s2;

	void transfer() {
		while (!s1.empty()) {
			s2.push(s1.top());
			s1.pop();
		}
	}

public:
    void enqueue(int num) {
        s1.push(num);
    }
    
    void dequeue() {
        if (s2.empty()) {
			transfer();
		}
		s2.pop();
    }
    
    int front() {
        if (s2.empty()) {
			transfer();
		}
		return s2.top();
    }
};

int main() {
    int queries;
    cin >> queries;
    
    Queue q;
    
    int query, num;
    while (queries--) {
        cin >> query;
        if (query == 1) {
            cin >> num;
            q.enqueue(num);
        } else if (query == 2) {
            q.dequeue();
        } else if (query == 3) {
            cout << q.front() << "\n";
        }
    }
    
    
    return 0;
}
