#include <bits/stdc++.h>

using namespace std;

string ltrim(const string &);
string rtrim(const string &);
vector<string> split(const string &);

/*
 * Complete the 'contacts' function below.
 *
 * The function is expected to return an INTEGER_ARRAY.
 * The function accepts 2D_STRING_ARRAY queries as parameter.
 */
// TODO: TRIE data structure

class TrieNode {
private:
	TrieNode* children[26] = {nullptr}; // number of alphabets
	int size = 0;

	void add(const string& str, int index) {
		++size;
		
		if (index == (int) str.size()) return;

		int i = str[index] - 'a';

		TrieNode* child = children[i];
		if (!child) {
			child = new TrieNode;
			children[i] = child;
		}

		child->add(str, index + 1);
	}

	int findCount(const string& str, int index) {
		if (index == (int) str.size()) return size;

		int i = str[index] - 'a';
		TrieNode* child = children[i];
		if (!child) return 0;

		return child->findCount(str, index + 1);
	}

	void deallocate(TrieNode* node) {
		for (auto child : children) {
			delete child;
		}
	}

public:
	TrieNode() {
		for (int i = 0; i < 26; ++i) {
			children[i] = nullptr;
		}
	}

	~TrieNode() {
		deallocate(this);
	}

	void add(const string& str) {
		add(str, 0);
	}

	int find(const string& str) {
		return findCount(str, 0);
	}
};

vector<int> contacts(vector<vector<string>> queries) {
	TrieNode node;
	vector<int> counts;
	for (auto& query : queries) {
		if (query[0] == "add") {
			node.add(query[1]);
		} else if (query[0] == "find") {
			counts.push_back(node.find(query[1]));
		}
	}
	return counts;
}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    string queries_rows_temp;
    getline(cin, queries_rows_temp);

    int queries_rows = stoi(ltrim(rtrim(queries_rows_temp)));

    vector<vector<string>> queries(queries_rows);

    for (int i = 0; i < queries_rows; i++) {
        queries[i].resize(2);

        string queries_row_temp_temp;
        getline(cin, queries_row_temp_temp);

        vector<string> queries_row_temp = split(rtrim(queries_row_temp_temp));

        for (int j = 0; j < 2; j++) {
            string queries_row_item = queries_row_temp[j];

            queries[i][j] = queries_row_item;
        }
    }

    vector<int> result = contacts(queries);

    for (size_t i = 0; i < result.size(); i++) {
        fout << result[i];

        if (i != result.size() - 1) {
            fout << "\n";
        }
    }

    fout << "\n";

    fout.close();

    return 0;
}

string ltrim(const string &str) {
    string s(str);

    s.erase(
        s.begin(),
        find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace)))
    );

    return s;
}

string rtrim(const string &str) {
    string s(str);

    s.erase(
        find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(),
        s.end()
    );

    return s;
}

vector<string> split(const string &str) {
    vector<string> tokens;

    string::size_type start = 0;
    string::size_type end = 0;

    while ((end = str.find(" ", start)) != string::npos) {
        tokens.push_back(str.substr(start, end - start));

        start = end + 1;
    }

    tokens.push_back(str.substr(start));

    return tokens;
}
