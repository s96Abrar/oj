#include <bits/stdc++.h>

using namespace std;

string ltrim(const string &);
string rtrim(const string &);

/*
 * Complete the 'runningMedian' function below.
 *
 * The function accepts INTEGER_ARRAY arr as parameter.
 */
#include <queue>
typedef priority_queue<int> SmallerPQ;
typedef priority_queue<int, vector<int>, std::greater<int>> LargerPQ;

void addNumber(int num, SmallerPQ& lower_nums, LargerPQ& higher_nums) {
    if (lower_nums.empty() || num < lower_nums.top()) {
        lower_nums.push(num);
    } else {
        higher_nums.push(num);
    }
}

void balance(SmallerPQ& lower_nums, LargerPQ& higher_nums) {
    if (lower_nums.size() > higher_nums.size()) {
        if (lower_nums.size() - higher_nums.size() >= 2) {
            higher_nums.push(lower_nums.top());
            lower_nums.pop();
        }
    } else {
        if (higher_nums.size() - lower_nums.size() >= 2) {
            lower_nums.push(higher_nums.top());
            higher_nums.pop();
        }
    }
}

double getMedian(SmallerPQ& lower_nums, LargerPQ& higher_nums) {
    if (lower_nums.empty() && higher_nums.empty()) {
        return 0.0;
    }
    
    if (lower_nums.size() == higher_nums.size()) {
        return 1.0 * (lower_nums.top() + higher_nums.top()) / 2;
    }
    
    if (lower_nums.size() > higher_nums.size()) {
        return 1.0 * lower_nums.top();
    }
    return 1.0 * higher_nums.top();
}

void runningMedian(vector<int> arr) {
    // Print your answer within the function
    SmallerPQ lower_nums;
    LargerPQ higher_nums;
    for (auto& num : arr) {
        addNumber(num, lower_nums, higher_nums);
        balance(lower_nums, higher_nums);
        printf("%.1f\n", getMedian(lower_nums, higher_nums));
    }
}

int main()
{
    string n_temp;
    getline(cin, n_temp);

    int n = stoi(ltrim(rtrim(n_temp)));

    vector<int> a(n);

    for (int i = 0; i < n; i++) {
        string a_item_temp;
        getline(cin, a_item_temp);

        int a_item = stoi(ltrim(rtrim(a_item_temp)));

        a[i] = a_item;
    }

    runningMedian(a);

    return 0;
}

string ltrim(const string &str) {
    string s(str);

    s.erase(
        s.begin(),
        find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace)))
    );

    return s;
}

string rtrim(const string &str) {
    string s(str);

    s.erase(
        find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(),
        s.end()
    );

    return s;
}
