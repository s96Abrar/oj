# STL Time complexity for operations
- std::vector
 - assign - O(n)
 - insert - O(n)
 - erase - O(n)
 - clear - O(n)

# Linux Basic Questions
- LILO - Linux Loader
- BASH - Bourne Again Shell
- Swap Space
- Process States - { New/Ready, Running, Blocked/Waiting, Terminated/Completed, Zombie }

# Commands
- rsync => Synchronize and transfer files (rsync -av <source> <dest>)

# Linux File Permissions
Read (r)
Write (w)
Execute (x)

User (u)
Group (g)
Others (o)
All (a)

Octal Notations
000 => 0 => ---
001 => 1 => --x
010 => 2 => -w-
011 => 3 => -wx
100 => 4 => r--
101 => 5 => r-x
110 => 6 => rw-
111 => 7 => rwx

# Linux Advance File Permissions
User_ID    => u + s => 4744
Group_ID   => g + s => 2744
Sticky Bit => +t

