#include <bits/stdc++.h>

using namespace std;

struct Coord {
	int x, y;
};

struct Wormhole {
	Coord start, end;
	int cost;
};

int distance(const Coord& c1, const Coord& c2) {
	return abs(c1.x - c2.x) + abs(c1.y - c2.y);
}

// shortest(cost, source, destination)
// min(minCost, cost + distance(source, destination))
// shortest(cost + worm[i].cost + distance(s, worm[i].start), worm[i].end, dest)
// shortest(cost + worm[i].cost + distance(s, worm[i].end), worm[i].start, dest)

void shortest(int cost, int size, int& minCost, 
	vector<bool>& visited, const Coord& s, const Coord& d, 
	const vector<Wormhole>& wormholes) 
{
	minCost = min(minCost, cost + distance(s, d));

	for (int i = 0; i < size; ++i) {
		if (!visited[i]) {
			visited[i] = true;
			// wormhole can be visited bi-directionally
			
			// s to wormhole start
			shortest(
				cost + wormholes[i].cost + distance(s, wormholes[i].start), 
				size, minCost, visited, wormholes[i].end, d, wormholes);
			
			// s to wormhole end
			shortest(
				cost + wormholes[i].cost + distance(s, wormholes[i].end), 
				size, minCost, visited, wormholes[i].start, d, wormholes);
			visited[i] = false;
		}
	}
}

int main()
{
	int t;
	cin >> t;
	while (t--) {
		int n;
		cin >> n;
		
		Coord s, d;
		vector<Wormhole> wormholes(n);

		cin >> s.x >> s.y;
		cin >> d.x >> d.y;

		for (int j = 0; j < n; ++j) {
			cin >> wormholes[j].start.x >> wormholes[j].start.y
				>> wormholes[j].end.x >> wormholes[j].end.y 
				>> wormholes[j].cost;
		}

		vector<bool> visited(n + 2, false);

		int minDistance = INT_MAX;
		shortest(0, n, minDistance, visited, s, d, wormholes);

		cout << minDistance << "\n";
	}
	return 0;
}
