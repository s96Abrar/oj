#include <iostream>
#include <string>

using namespace std;

int toNum(const char* str, int n) {
	int num = 0;
	int level = 1;
	while (n--) {
		num += (str[n] - '0') * level;
		level *= 10;
	}
	return num;
}

int main()
{
	int k;
	cin >> k;

	string str;
	cin >> str;

	int n = (int) str.size();
	int level = -1;
	int sum = 0;

	for (int i = 0; i < n; i++) {
		if (str[i] == '(') {
			++level;
		} else if (str[i] == ')') {
			--level;
		} else if (level == k) {
			string num;

			while (i < n && str[i] != '(' && str[i] != ')') {
				num += str[i++];
			}
			--i;

			sum += toNum(num.c_str(), num.size());
		}
	}

	cout << sum << "\n";

	return 0;
}