#include <iostream>
#include <vector>

typedef long long int ll;
using namespace std;

ll coins[101];

ll knapsack(ll n, ll w) {
	ll dp[999999] = {0};
	for (ll i = 1; i <= n; i++) {
		for (ll j = w; j > 0; j--) {
			if (coins[i] <= j) {
				dp[j] = max(dp[j], coins[i] + dp[j - coins[i]]);
			}
		}
	}
	return dp[w];
}

int main() 
{
	ll t;
	cin >> t;
	while (t--) {
		ll n;
		cin >> n;

		ll sum = 0;
		for (ll i = 1; i <= n; ++i) {
			cin >> coins[i];
			sum += coins[i];
		}

		cout << (sum - 2 * knapsack(n, sum / 2)) << "\n";
	}
	return 0;
}