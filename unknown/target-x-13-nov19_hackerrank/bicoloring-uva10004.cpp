#include <iostream>
#include <vector>
#include <queue>

using namespace std;

bool isBipartite(int n, const vector<vector<int>>& adj) {
	int colors[n] = {0}; // odd = 1, even = -1

	queue<int> q;

	for (int i = 0; i < n; i++) {
		if (colors[i] == 0) {
			
			// BFS
			q.push(i);
			colors[i] = -1;

			while (!q.empty()) {
				int node = q.front();
				q.pop();

				for (auto& neighbour : adj[node]) {
					// Should Not be in same color
					if (colors[neighbour] == colors[node]) {
						return false;
					} else if (colors[neighbour] == 0) {
						q.push(neighbour);
						colors[neighbour] = -1 * colors[node];
					}
				}
			}
		}
	}
	return true;
}

int main()
{
	int n;
	while (cin >> n) {
		if (n == 0) break;

		vector<vector<int>> adj(n, vector<int>());
		int l, u, v;

		cin >> l;
		for (int i = 0; i < l; i++) {
			cin >> u >> v;
			adj[u].push_back(v);
			adj[v].push_back(u);
		}

		if (isBipartite(n, adj)) {
			cout << "BICOLORABLE.\n";
		} else {
			cout << "NOT BICOLORABLE.\n";
		}
	}

	return 0;
}