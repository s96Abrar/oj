#include <bits/stdc++.h>

using namespace std;

int n;
int gates[3];
int people[3];
int totalPeople = 0;
int minCost = INT_MAX;

void solver(int spot, int cost, int count) {
	if (count == totalPeople) {
		minCost = min(minCost, cost);
		return;
	}

	if (spot == n + 1) return;

	for (int i = 0; i < 3; ++i) {
		if (people[i] > 0) {
			people[i]--;
			solver(spot + 1, cost + abs(gates[i] - spot) + 1, count + 1);
			people[i]++;
		}
	}

	solver(spot + 1, cost, count);
}

int main()
{
	cin >> n;
	
	for (int i = 0; i < 3; ++i) {
		cin >> gates[i];
	}

	for (int i = 0; i < 3; ++i) {
		cin >> people[i];
		totalPeople += people[i];
	}

	solver(1, 0, 0);

	cout << minCost << "\n";

	return 0;
}