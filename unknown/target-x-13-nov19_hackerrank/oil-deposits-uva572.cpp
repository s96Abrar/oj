#include <iostream>
#include <string>

using namespace std;

string g[100];
bool visited[100][100];
int m, n;

bool isValid(int i, int j) {
	return i >= 0 && i < m && j >= 0 && j < n && g[i][j] == '@';
}

int locations[8][2] = {
	{-1, -1},
	{-1,  0},
	{-1,  1},
	{ 1, -1},
	{ 1,  0},
	{ 1,  1},
	{ 0, -1},
	{ 0,  1},
};

void dfs(int i, int j) {
	if (!isValid(i, j) || visited[i][j]) return;

	g[i][j] = '*';
	visited[i][j] = true;
	for (int k = 0; k < 8; k++) {
		dfs(i + locations[k][0], j + locations[k][1]);
	}
}

int main()
{
	while (cin >> m >> n) {
		if (m == 0) break;

		string str;
		for (int i = 0; i < m; i++) {
			cin >> str;
			g[i] = str;
			for (int j = 0; j < n; j++) {
				visited[i][j] = false;
			}
		}

		int count = 0;
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				// cout << g[i][j];
				if (g[i][j] == '@') {
					dfs(i, j);
					count++;
				}
			}
			// cout << "\n";
		}

		cout << count << "\n";
	}
	return 0;
}
