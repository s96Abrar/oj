#include <bits/stdc++.h>

using namespace std;

int solver(int n, int l, int r, const vector<int>& nums, vector<vector<int>>& dp) {
	if (l > r) return 0;
	if (dp[l][r] != -1) return dp[l][r];

	dp[l][r] = 0;
	for (int i = l; i <= r; ++i) {
		int score = nums[l - 1] * nums[i] * nums[r + 1];
		score += solver(n, l, i - 1, nums, dp) + solver(n, i + 1, r, nums, dp);
		dp[l][r] = max(dp[l][r], score);
	}
	return dp[l][r];
}

int main()
{
	int n;
	cin >> n;

	vector<int> nums(n + 2, 1);
	for (int i = 1; i <= n; ++i) {
		cin >> nums[i];
	}

	vector<vector<int>> dp(n + 2, vector<int>(n + 2, -1));

	cout << solver(n, 1, n, nums, dp) << "\n";
	
	return 0;
}