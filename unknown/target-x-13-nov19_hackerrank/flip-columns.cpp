// #include <bits/stdc++.h>
#include <iostream>
#include <string>

using namespace std;

int main()
{
	int m, n, k;
	// unordered_map<string, int> freq;
	int counts[15] = {0};
	string board[15];

	cin >> m >> n >> k;
	for (int i = 0; i < m; ++i) {
		string row = "";
		int countZeros = 0, val;
		for (int j = 0; j < n; ++j) {
			cin >> val;
			
			if (val == 0) ++countZeros;

			row += ('0' + val);
		}
		//
		board[i] = row;

		if (k >= countZeros && (k - countZeros) % 2 == 0) {
			//
			counts[i]++;
			// ++freq[row];
		}
	}

	//
	int maxFreq = -10000;
	for (int i = 0; i < m; i++) {
		int f = counts[i];
		counts[i] = 0;

		for (int j = 0; j < m; j++) {
			if (i != j && board[i] == board[j]) {
				f += counts[j];
				counts[j] = 0;
			}
		}
		maxFreq = max(maxFreq, f);
	}

	// int maxFreq = INT_MIN;
	// for (auto &f : freq) {
	// 	maxFreq = max(maxFreq, f.second);
	// }
	cout << maxFreq << "\n";

	return 0;
}

/*
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <unordered_map>
using namespace std;


int main() {
    unordered_map<string, int> freq;
    int n, m, k, num;
    
    cin >> n >> m >> k;
    for (int i = 0; i < n; ++i) {
        string row;
        int zeros = 0;
        for (int j = 0; j < m; ++j) {
            cin >> num;
            if (num == 0) ++zeros;
            row += (num + '0');
        }
        
        if (k >= zeros && (k - zeros) % 2 == 0) {
            ++freq[row];
        }
    }
    
    int maxFreq = -99999;
    for (auto &f : freq) {
        maxFreq = max(maxFreq, f.second);
    }
    
    cout << maxFreq << "\n";
    
    return 0;
}

*/