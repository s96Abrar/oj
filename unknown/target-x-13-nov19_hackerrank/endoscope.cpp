/*#include <bits/stdc++.h>

using namespace std;

struct Cell {
	int x, y, l;
};

bool isValid(int m, int n, int x, int y) {
	return (x >= 0 && x < m && y >= 0 && y < n);
}

bool left(int type) {
	return type == 1 || type == 3 || type == 6 || type == 7;
}

bool right(int type) {
	return type == 1 || type == 3 || type == 4 || type == 5;
}

bool up(int type) {
	return type == 1 || type == 2 || type == 4 || type == 7;
}

bool down(int type) {
	return type == 1 || type == 2 || type == 5 || type == 6;
}

int visitedPipes(int m, int n, int x, int y, int l, const vector<vector<int>>& w_map) {
	vector<vector<int>> visited(m, vector<int>(n, 0));

	queue<Cell> q;
	visited[x][y] = 1;
	q.push({x, y, l});

	int count = 0;
	
	while (!q.empty()) {
		Cell c = q.front();
		q.pop();

		x = c.x;
		y = c.y;
		l = c.l;

		if (l == 0) continue;
		
		++count;
		
		if (isValid(m, n, x - 1, y) && up(w_map[x][y]) && down(w_map[x - 1][y]) && !visited[x - 1][y]) {
			visited[x - 1][y] = 1;
			q.push({x - 1, y, l - 1});
		}
		if (isValid(m, n, x + 1, y) && down(w_map[x][y]) && up(w_map[x + 1][y]) && !visited[x + 1][y]) {
			visited[x + 1][y] = 1;
			q.push({x + 1, y, l - 1});
		}
		if (isValid(m, n, x, y - 1) && left(w_map[x][y]) && right(w_map[x][y - 1]) && !visited[x][y - 1]) {
			visited[x][y - 1] = 1;
			q.push({x, y - 1, l - 1});
		}
		if (isValid(m, n, x, y + 1) && right(w_map[x][y]) && left(w_map[x][y + 1]) && !visited[x][y + 1]) {
			visited[x][y + 1] = 1;
			q.push({x, y + 1, l - 1});
		}
	}
	return count;
}

int main() 
{
	int t;
	cin >> t;
	
	while (t--) {
		int m, n, x, y, l;

		cin >> m >> n >> x >> y >> l;

		vector<vector<int>> w_map(m, vector<int>(n, 0));
		for (int i = 0; i < m; ++i) {
			for (int j = 0; j < n; ++j) {
				cin >> w_map[i][j];
			}
		}

		if (w_map[x][y] > 0) {
			std::cout << visitedPipes(m, n, x, y, l, w_map) << "\n";
		} else std::cout << "0\n";
	}
	return 0;
}
*/

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <queue>
using namespace std;

struct Cell {
    int row, col, len;
};

int map[50][50];
bool visited[50][50];
int t, m, n;

Cell start;

bool isValid(int row, int col) {
    return (row >= 0 && row < m && col >= 0 && col < n);
}

bool left(int type) {
	return type == 1 || type == 3 || type == 6 || type == 7;
}

bool right(int type) {
	return type == 1 || type == 3 || type == 4 || type == 5;
}

bool up(int type) {
	return type == 1 || type == 2 || type == 4 || type == 7;
}

bool down(int type) {
	return type == 1 || type == 2 || type == 5 || type == 6;
}

int bfs() {
	if (map[start.row][start.col] == 0) return 0;

    int maxVisit = 0;
    
    queue<Cell> q;
    q.push(start);

	visited[start.row][start.col] = true;
    
    while (!q.empty()) {
        Cell c = q.front();
        q.pop();
        
        if (c.len <= 0) continue;
    	++maxVisit;
        
        if (isValid(c.row - 1, c.col) && !visited[c.row - 1][c.col] && up(map[c.row][c.col]) && down(map[c.row - 1][c.col])) {
			q.push({c.row - 1, c.col, c.len - 1});
			visited[c.row - 1][c.col] = true;
		}

		if (isValid(c.row + 1, c.col) && !visited[c.row + 1][c.col] && down(map[c.row][c.col]) && up(map[c.row + 1][c.col])) {
			q.push({c.row + 1, c.col, c.len - 1});
			visited[c.row + 1][c.col] = true;
		}

		if (isValid(c.row, c.col - 1) && !visited[c.row][c.col - 1] && left(map[c.row][c.col]) && right(map[c.row][c.col - 1])) {
			q.push({c.row, c.col - 1, c.len - 1});
			visited[c.row][c.col - 1] = true;
		}

		if (isValid(c.row, c.col + 1) && !visited[c.row][c.col + 1] && right(map[c.row][c.col]) && left(map[c.row][c.col + 1])) {
			q.push({c.row, c.col + 1, c.len - 1});
			visited[c.row][c.col + 1] = true;
		}
    }
    return maxVisit;
}

int main() {
    cin >> t;
    
    while (t--) {
        cin >> m >> n >> start.row >> start.col >> start.len;
        
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                cin >> map[i][j];

            	visited[i][j] = false;
            }
        }
        
        cout << bfs() << "\n";
    }
    return 0;
}
