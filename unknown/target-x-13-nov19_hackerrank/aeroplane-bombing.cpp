#include <bits/stdc++.h>

using namespace std;

int maxCoins = INT_MIN;

void getMaxCoins(int row, int col, int coins, int isRowSafe, bool boomUsed, const vector<vector<int>>& board) {
	if (row < 0 || col < 0 || col >= 5) {
		maxCoins = max(maxCoins, coins);
		return;
	}

	int newCoins = coins;

	if (board[row][col] == 1 || board[row][col] == 0) {
		newCoins = coins + board[row][col];
		
		if (boomUsed) --isRowSafe;
	} else {
		if (boomUsed) {
			if (isRowSafe <= 0) {
				maxCoins = max(maxCoins, coins);
				return;
			}

			--isRowSafe;
		} else {
			boomUsed = true;
			isRowSafe = 4;
		}
	}

	getMaxCoins(row - 1, col, newCoins, isRowSafe, boomUsed, board);
	getMaxCoins(row - 1, col + 1, newCoins, isRowSafe, boomUsed, board);
	getMaxCoins(row - 1, col - 1, newCoins, isRowSafe, boomUsed, board);
}

int main()
{
	int t;
	cin >> t;
	for (int k = 1; k <= t; ++k) {
		maxCoins = INT_MIN;

		int n;
		cin >> n;

		vector<vector<int>> board(n, vector<int>(5, 0));
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < 5; ++j) {
				cin >> board[i][j];
			}
		}

		getMaxCoins(n - 1, 1, 0, 0, false, board);
		getMaxCoins(n - 1, 2, 0, 0, false, board);
		getMaxCoins(n - 1, 3, 0, 0, false, board);

		cout << "#" << k << " " << maxCoins << "\n";
	}
	return 0;
}