#include <bits/stdc++.h>

using namespace std;

struct Coord {
	int x, y;
};

int distance(const Coord& c1, const Coord& c2) {
	return abs(c1.x - c2.x) + abs(c1.y - c2.y);
}

void calcShortest(int index, int count, int size, int cost, int& minCost, vector<bool>& visited, const vector<vector<int>>& dist) {
	if (count == size) {
		minCost = min(minCost, cost + dist[index][size + 1]);
		return;
	}

	for (int i = 1; i <= size; ++i) {
		if (!visited[i]) {
			visited[i] = true;
			calcShortest(i, count + 1, size, cost + dist[index][i], minCost, visited, dist);
			visited[i] = false;
		}
	}
}

int main()
{
	int t = 10;
	for (int i = 1; i <= t; ++i) {
		int n;
		cin >> n;
		
		vector<Coord> customers(n + 2);

		cin >> customers[0].x >> customers[0].y;
		cin >> customers[n + 1].x >> customers[n + 1].y;

		for (int j = 1; j <= n; ++j) {
			cin >> customers[j].x >> customers[j].y;
		}

		vector<bool> visited(n + 2, false);
		vector<vector<int>> dist(n + 2, vector<int>(n + 2, 0));
		for (int j = 0; j < n + 2; ++j) {
			for (int k = 0; k < n + 2; ++k) {
				dist[j][k] = distance(customers[j], customers[k]);
				dist[k][j] = distance(customers[j], customers[k]);
			}
		}

		int minDistance = INT_MAX;
		calcShortest(0, 0, n, 0, minDistance, visited, dist);

		cout << "# " << i << " " << minDistance << "\n";
	}
	return 0;
}
