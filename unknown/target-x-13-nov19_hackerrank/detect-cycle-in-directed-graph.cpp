#include <iostream>
#include <algorithm>

using namespace std;

const int MAX_N = 11;

int n;

bool g[MAX_N][MAX_N];

int answer[MAX_N];
int answerSize;

int stack[MAX_N];
int top = -1;

int sumCycle = 1000;

/*

// For undirected graph

#include <vector>
bool dfs(int v, const vector<vector<int>>& adj, bool visited[], int parent) {
	visited[v] = true;
	for (int nei : adj[v]) {
		if (!visited[nei]) {
			if (dfs(nei, adj, visited, nei)) return true;
		} else if (parent != nei) {
			return true;
		}
	}
	return false;
}

bool isCycle(int v, const vector<vector<int>>& adj) {
	bool visited[v];
	for (int i = 0; i < v; i++) {
		if (!visited[i]) {
			if (dfs(i, adj, visited, -1)) return true;
		}
	}
	return false;
}
*/

void detectCycle(int index, bool* visited) {
	if (visited[index] && top != -1) {
		int i, localSum = stack[top];
		
		// Calculating the current cycle count
		for (i = top - 1; i >= 0 && stack[i] != index; --i) {
			localSum += stack[i];
		}

		// Updating the answer
		if (i != -1 && localSum < sumCycle) {
			sumCycle = localSum;
			answerSize = -1;
			answer[++answerSize] = stack[top];
			for (i = top - 1; i >= 0 && stack[i] != index; --i) {
				answer[++answerSize] = stack[i];
			}
		}
		return;
	}

	visited[index] = true;
	for (int i = 1; i <= n; ++i) {
		if (g[index][i]) {
			stack[++top] = i; // push
			detectCycle(i, visited);
			--top; // pop
		}
	}
	visited[index] = false;
}

void checkCycle() {
	bool visited[n + 1] = {false};
	
	for (int i = 1; i <= n; ++i) {
		if (!visited[i]) {
			stack[++top] = i; // push
			detectCycle(i, visited);
			--top; // pop
		}
	}

	sort(answer, answer + answerSize + 1);
	for (int i = 0; i <= answerSize; ++i) {
		cout << answer[i] << " ";
	}
	cout << "\n";
}

int main()
{
	for (int i = 0; i < MAX_N; ++i) {
		for (int j = 0; j < MAX_N; ++j) {
			g[i][j] = false;
		}
	}

	int m;
	cin >> n >> m;

	int u, v;
	for (int i = 0; i < m; ++i) {
		cin >> u >> v;
		g[u][v] = true;
	}

	checkCycle();

	return 0;
}