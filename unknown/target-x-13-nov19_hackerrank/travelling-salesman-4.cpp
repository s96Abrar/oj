#include <bits/stdc++.h>

using namespace std;

const int MAX_N = 12;

void solve(int index, int count, int size, int cost, int& minCost, vector<int>& visited, const vector<vector<int>>& dist) {
	if (count + 1 == size) {
		minCost = min(minCost, cost + dist[index][0]);
		return;
	}

	visited[index] = 1;
	for (int i = 0; i < size; i++) {
		if (!visited[i]) {
			solve(i, count + 1, size, cost + dist[index][i], minCost, visited, dist);
		}
	}
	visited[index] = 0;
}

int main()
{
	int t, n;
	cin >> t;
	while (t--) {
		cin >> n;

		vector<int> visited(MAX_N, 0);
		vector<vector<int>> dist(n, vector<int>(n));
		int minCost = INT_MAX;

		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < n; ++j) {
				cin >> dist[i][j];
			}
		}

		solve(0, 0, n, 0, minCost, visited, dist);
		cout << minCost << "\n";
	}
	return 0;
}

/*
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

bool visited[12];
int dist[12][12];
    
void calculateMinCost(int index, int n, int count, int cost, int& minCost) {
    if (count + 1 == n) {
        minCost = min(minCost, cost + dist[index][0]);
        return;
    }
    
    visited[index] = true;
    for (int i = 0; i < n; ++i) {
        if (!visited[i]) {
            calculateMinCost(i, n, count + 1, cost + dist[index][i], minCost);
        }
    }
    visited[index] = false;
}

int main() {
    int t;
    cin >> t;
    while (t--) {
        int n;
        cin >> n;
        
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                cin >> dist[i][j];
            }
            
            visited[i] = false;
        }
        
        int minCost = 9999999;
        calculateMinCost(0, n, 0, 0, minCost);
        cout << minCost << "\n";
        
    }
    return 0;
}

*/