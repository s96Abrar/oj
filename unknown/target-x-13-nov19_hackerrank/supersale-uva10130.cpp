#include <iostream>

using namespace std;

int P[1001];
int W[1001];
int MG[101];

// dp[i, w] = max(dp[i - 1, w], v[i - 1] + dp[i - 1, w - W[i - 1]])

int knapsack(int n, int w) {
	int dp[n + 1][w + 1];

	for (int i = 0; i <= n; i++) {
		for (int j = 0; j <= w; j++) {
			if (i == 0 || j == 0) {
				dp[i][j] = 0;
			} else if (W[i - 1] <= j) {
				dp[i][j] = max(dp[i - 1][j], P[i - 1] + dp[i - 1][j - W[i - 1]]);
			} else {
				dp[i][j] = dp[i - 1][j];
			}
		}
	}
	return dp[n][w];
}

int main()
{
	int t;
	cin >> t;
	while (t--) {
		int n, g;
		
		cin >> n;
		for (int i = 0; i < n; i++) {
			cin >> P[i] >> W[i];
		}

		cin >> g;
		for (int i = 0; i < g; i++) {
			cin >> MG[i];
		}

		int ans = 0;
		for (int i = 0; i < g; i++) {
			ans += knapsack(n, MG[i]);
		}
		cout << ans << "\n";
	}
	return 0;
}