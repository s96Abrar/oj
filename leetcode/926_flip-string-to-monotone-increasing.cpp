#include <iostream>
#include <string>

using namespace std;

int main() 
{
	string str;
	cin >> str;

	int result = 0;
	int countOne = 0;
	for (char ch : str) {
		if (ch == '1') {
			countOne++;
		} else {
			result = min(result + 1, countOne);
		}
	}

	cout << result << "\n";

	return 0;
}