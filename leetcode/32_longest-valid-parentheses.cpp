class Solution {
public:
    int longestValidParentheses(string s) {
        stack<int> parenthesis;
        int count = 0;
        int i = 0, len = (int) s.length();
        
        parenthesis.push(-1);
        
        while (i < len) {
            if (s[i] == '(') {
                parenthesis.push(i);
            } else {
                parenthesis.pop();
                if (parenthesis.empty()) {
                    parenthesis.push(i);
                } else {
                    count = max(i - parenthesis.top(), count);
                }
            }
            ++i;
        }
        return count;
    }
};
