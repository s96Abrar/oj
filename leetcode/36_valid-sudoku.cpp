class Solution {
public:
    bool isValid(const vector<vector<char>>& board, int row, int col, char value) {
        for (int i = 0; i < 9; ++i) {
            if (board[row][i] == value) return false;
            if (board[i][col] == value) return false;
            if (board[3 * (row / 3) + i / 3][3 * (col / 3) + i % 3] == value) return false;
        }
        return true;
    }

    bool isValidSudoku(vector<vector<char>>& board) {
        for (int row = 0; row < 9; ++row) {
            for (int col = 0; col < 9; ++col) {
                if (board[row][col] != '.') {
                    char ch = board[row][col];
                    board[row][col] = '.';

                    if (!isValid(board, row, col, ch)) return false;

                    board[row][col] = ch;
                }
            }
        }
        return true;
    }
};

