class Solution {
public:
    bool isValid(const vector<vector<char>>& board, int row, int col, char value) {
        for (int i = 0; i < 9; ++i) {
            if (board[row][i] == value) return false;
            if (board[i][col] == value) return false;
            if (board[3 * (row / 3) + i / 3][3 * (col / 3) + i % 3] == value) return false;
        }
        return true;
    }
    
    bool solveHelper(int row, int col, vector<vector<char>>& board) {
        if (row == 9) return true;
        if (col == 9) return solveHelper(row + 1, 0, board);
        if (board[row][col] != '.') return solveHelper(row, col + 1, board);
        
        for (char ch = '1'; ch <= '9'; ++ch) {
            if (isValid(board, row, col, ch)) {
                board[row][col] = ch;

                if (solveHelper(row, col + 1, board)) return true;

                board[row][col] = '.';
            }
        }
        return false;
    }

    void solveSudoku(vector<vector<char>>& board) {
        solveHelper(0, 0, board);
    }
};
