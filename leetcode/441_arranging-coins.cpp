#include <iostream>

using namespace std;

int binarySearch(int n) {
	int l = 1, r = n;
	int result = 0;
	while (l <= r) {
		int m = (l + r) / 2;
		int coins = (m * (m + 1)) / 2;

		if (coins > n) {
			r = m - 1;
		} else {
			l = m + 1;
			result = max(result, m);
		}
	}
	return result;
}

#include <cmath>
int main() 
{
	int n;
	cin >> n;

	// Math formula => ( R (R + 1) ) / 2 = n
	// Find number of rows R
	// int R = (-1 + sqrt(1 + 8 * n)) / 2;
	// cout << R << "\n";
	
	cout << binarySearch(n) << "\n";
	
	return 0;
}